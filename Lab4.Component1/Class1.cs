﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1 
{
    public class Class1 : AbstractComponent
    {
        Interfejs interfejs;
        public Class1() 
        {
            RegisterRequiredInterface<Interfejs>();
        }
        public Class1(Interfejs interfejs)
        {
            this.interfejs = interfejs;
            RegisterProvidedInterface<Interfejs>(this.interfejs);
        }
        public void MetodaA1()
        {
            interfejs.MetodaA();
        }

        public void MetodaB2()
        {
            interfejs.MetodaA();
        }

        public override void InjectInterface(Type type, object impl)
        {

        }
    }
}
