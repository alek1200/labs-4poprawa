﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class Class1: AbstractComponent, Interfejs
    {
        public Class1()
        {
            RegisterProvidedInterface<Interfejs>(this);
        }

        public void MetodaA()
        {
            Console.WriteLine("jestem metodaA z Component2");
        }

        public void MetodaB()
        {
            Console.WriteLine("jestem metodaB z Component2");
        }

        public override void InjectInterface(Type type, object impl)
        {
            
        }
    }
}
