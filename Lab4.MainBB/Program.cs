﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component2;
using Lab4.Component2B;
using Lab4.Contract;


namespace Lab4.MainBB
{
    class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();
            Component2.Class1 component2 = new Component2.Class1();
            Component2B.Class2 component2B = new Component2B.Class2();
            component2.RegisterProvidedInterface<Interfejs>(component2);
            component2B.RegisterProvidedInterface<Interfejs>(component2B);
            kontener.RegisterComponent(component2);
            kontener.RegisterComponent(component2B);
            component2.MetodaA();
            component2B.MetodaA();
        }
    }
}
