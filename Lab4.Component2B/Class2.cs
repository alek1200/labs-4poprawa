﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Class2:AbstractComponent, Interfejs
    {
        public void MetodaA()
        {
            int c = 1;
            Console.WriteLine(c);
        }

        public void MetodaB()
        {
            int a = 2;
            Console.WriteLine(a);
        }

        public Class2()
        {
            this.RegisterProvidedInterface(typeof(Interfejs), this);
        }

        public override void InjectInterface(Type type, object impl)
        {
            
        }
    }
}
