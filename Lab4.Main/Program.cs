﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();
            Component2.Class1 component1 = new Component2.Class1();
            Component2B.Class2 component2 = new Component2B.Class2();
            component2.RegisterProvidedInterface<Interfejs>(component2);
            kontener.RegisterComponent(component1);
            kontener.RegisterComponent(component2);
            component1.MetodaA();
            component2.MetodaA();
        }
    }
}
