﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;


namespace Lab4.Contract
{
    public interface Interfejs
    {
        void MetodaA();
        void MetodaB();
    }
}
